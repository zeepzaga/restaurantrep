﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Controls
{
    /// <summary>
    /// Логика взаимодействия для RatingControl.xaml
    /// </summary>
    public partial class RatingControl : UserControl
    {
        public static int selected;

        public RatingControl()
        {
            InitializeComponent();
            selected = 0;

        }

        private void TbkStar1_MouseEnter(object sender, MouseEventArgs e)
        {
            TbkStar1.Text = "\xE735";
            TbkStar2.Text = "\xE734";
            TbkStar3.Text = "\xE734";
            TbkStar4.Text = "\xE734";
            TbkStar5.Text = "\xE734";
        }

        private void TbkStar2_MouseEnter(object sender, MouseEventArgs e)
        {
            TbkStar1.Text = "\xE735";
            TbkStar2.Text = "\xE735";
            TbkStar3.Text = "\xE734";
            TbkStar4.Text = "\xE734";
            TbkStar5.Text = "\xE734";
        }

        private void TbkStar3_MouseEnter(object sender, MouseEventArgs e)
        {
            TbkStar1.Text = "\xE735";
            TbkStar2.Text = "\xE735";
            TbkStar3.Text = "\xE735";
            TbkStar4.Text = "\xE734";
            TbkStar5.Text = "\xE734";
        }

        private void TbkStar4_MouseEnter(object sender, MouseEventArgs e)
        {
            TbkStar1.Text = "\xE735";
            TbkStar2.Text = "\xE735";
            TbkStar3.Text = "\xE735";
            TbkStar4.Text = "\xE735";
            TbkStar5.Text = "\xE734";
        }

        private void TbkStar5_MouseEnter(object sender, MouseEventArgs e)
        {
            TbkStar1.Text = "\xE735";
            TbkStar2.Text = "\xE735";
            TbkStar3.Text = "\xE735";
            TbkStar4.Text = "\xE735";
            TbkStar5.Text = "\xE735";
        }

        private void TbkStar_MouseLeave(object sender, MouseEventArgs e)
        {
            TbkStar1.Text = "\xE734";
            TbkStar2.Text = "\xE734";
            TbkStar3.Text = "\xE734";
            TbkStar4.Text = "\xE734";
            TbkStar5.Text = "\xE734";
            Select();

        }

        private void TbkStar1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            selected = 1;
        }

        private void TbkStar2_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            selected = 2;
        }

        private void TbkStar3_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            selected = 3;
        }

        private void TbkStar4_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            selected = 4;
        }

        private void TbkStar5_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            selected = 5;
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            Select();
        }

        private void Select()
        {
            switch (selected)
            {
                case 1:
                    TbkStar1.Text = "\xE735";
                    break;
                case 2:
                    TbkStar1.Text = "\xE735";
                    TbkStar2.Text = "\xE735";
                    break;
                case 3:
                    TbkStar1.Text = "\xE735";
                    TbkStar2.Text = "\xE735";
                    TbkStar3.Text = "\xE735";
                    break;
                case 4:
                    TbkStar1.Text = "\xE735";
                    TbkStar2.Text = "\xE735";
                    TbkStar3.Text = "\xE735";
                    TbkStar4.Text = "\xE735";
                    break;
                case 5:
                    TbkStar1.Text = "\xE735";
                    TbkStar2.Text = "\xE735";
                    TbkStar3.Text = "\xE735";
                    TbkStar4.Text = "\xE735";
                    TbkStar5.Text = "\xE735";
                    break;
            }
        }
    }
}
