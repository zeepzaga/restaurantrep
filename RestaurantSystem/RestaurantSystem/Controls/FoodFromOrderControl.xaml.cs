﻿using RestaurantSystem.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Controls
{
    /// <summary>
    /// Логика взаимодействия для FoodFromOrderControl.xaml
    /// </summary>
    public partial class FoodFromOrderControl : UserControl
    {
        public FoodFromOrderControl()
        {
            InitializeComponent();
        }

        private void TbxCount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Any(c => !char.IsDigit(c)))
                e.Handled = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)    //Плюс
        {
            TbxCount.Text = $"{Convert.ToInt32(TbxCount.Text) + 1}";
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)   //Минус
        {
            int count = Convert.ToInt32(TbxCount.Text) - 1;
            if (count != 0)
            {
                TbxCount.Text = $"{count}";
            }

        }

        private void TbxCount_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                AppData.Context.ChangeTracker.Entries<OrderOfFood>().Where(p => p.Entity.Order == AppData.UserOrder).ToList().ForEach(p => p.Reload());
                int count = Convert.ToInt32(TbxCount.Text);
                var Orderfood = (sender as TextBox).DataContext as OrderOfFood;
                var foodOnOrder = AppData.Context.OrderOfFoods.ToList().FirstOrDefault(p => p.Order == AppData.UserOrder && p.Food == Orderfood.Food);
                if (TbxCount.Text == "0")
                {
                    TbxCount.Text = "1";
                }
                foodOnOrder.Count = count;
                if (TotalCount(AppData.Context.OrderOfFoods.ToList().Where(p => p.Order.Id == AppData.UserOrder.Id).ToList()) > 99)
                {
                    MessageBox.Show("Привышен лимит количества продуктов в заказе", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    while (AppData.Context.OrderOfFoods.ToList().FirstOrDefault(p => p.Order == AppData.UserOrder).Count > 99)
                    {
                        foodOnOrder.Count--;
                        AppData.Context.SaveChanges();
                    }
                    TbxCount.Text = foodOnOrder.Count.ToString();
                    return;
                }
                foodOnOrder.Count = count;
                AppData.Context.SaveChanges();
            }
            catch { }
        }
        private void TbxCount_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                var Orderfood = (sender as TextBox).DataContext as OrderOfFood;
                var foodOnOrder = AppData.Context.OrderOfFoods.ToList().FirstOrDefault(p => p.Order == AppData.UserOrder && p.Food == Orderfood.Food);
                if (String.IsNullOrWhiteSpace(TbxCount.Text))
                {
                    TbxCount.Text = foodOnOrder.Count.ToString();
                }
            }
            catch { }
        }

        private void TblockDelete_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var Orderfood = (sender as TextBlock).DataContext as OrderOfFood;
            var foodOnOrder = AppData.Context.OrderOfFoods.ToList().FirstOrDefault(p => p.Order == AppData.UserOrder && p.Food == Orderfood.Food);
            if (MessageBox.Show("Вы уверены, что хотите удалить блюдо из корзины?", "Вы уверены?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                AppData.Context.OrderOfFoods.Remove(foodOnOrder);
                AppData.Context.SaveChanges();
            }
        }
        private int TotalCount(List<OrderOfFood> list)
        {
            int count = 0;
            foreach (var food in list)
            {
                count += Convert.ToInt32(food.Count);
            }
            return count;
        }
    }
}
