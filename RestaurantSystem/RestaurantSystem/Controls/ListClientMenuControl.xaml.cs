﻿using RestaurantSystem.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Controls
{
    /// <summary>
    /// Логика взаимодействия для ListClientMenuControl.xaml
    /// </summary>
    public partial class ListClientMenuControl : UserControl
    {
        public ListClientMenuControl()
        {
            InitializeComponent();
        }

        private void BtnAddToOrder_Click(object sender, RoutedEventArgs e)
        {
            if (AppData.UserOrder == null)
            {
                AppData.UserOrder = new Order
                {
                    Client = AppData.Context.Clients.ToList().FirstOrDefault(p => p.UserId == AppData.CurrentUser.Id),
                    DateTimeOfStart = DateTime.Now,
                    StatusOfOrderId = 1,
                };
                AppData.Context.Orders.Add(AppData.UserOrder);
                AppData.Context.SaveChanges();
            }
            OrderOfFood orderOfFood = AppData.Context.OrderOfFoods.ToList().FirstOrDefault(p => p.Food == (BtnAddToOrder.DataContext as Food) && p.Order == AppData.UserOrder);
            if (orderOfFood != null)
            {
                orderOfFood.Count += 1;
                AppData.Context.SaveChanges();
            }
            else
            {
                AppData.UserOrder.OrderOfFoods.Add(new OrderOfFood
                {
                    Food = BtnAddToOrder.DataContext as Food,
                    Count = 1,
                    Order = AppData.UserOrder
                });
                AppData.Context.SaveChanges();
            }
            //if (AppData.UserOrder.Count == 99) return;
        }
    }
}
