﻿using RestaurantSystem.Entities;
using RestaurantSystem.Pages;
using RestaurantSystem.Pages.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace RestaurantSystem
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            timer.Tick += Timer_Tick;
            timer.Start();
            AppData.MainFrame = MainFrame;
            AppData.MainFrame.Navigate(new MenuPage());
            AppData.CurrentUser = AppData.Context.Users.FirstOrDefault(i => i.Login == Properties.Settings.Default.login && i.Password == Properties.Settings.Default.password);

            try
            {
                if (AppData.CurrentUser != null)
                {
                    switch (AppData.CurrentUser.Role.Id)
                    {
                        case "A":
                            AppData.MainFrame.Navigate(new Pages.Admin.MenuPage());
                            break;
                        case "Cl":
                            AppData.UserOrder = AppData.Context.Orders.FirstOrDefault(p => p.Client.User.Id == AppData.CurrentUser.Id && p.StatusOfOrderId == 1);
                            AppData.MainFrame.Navigate(new Pages.Client.MenuPage());
                            break;
                    }
                }
            }
            catch (Exception) { }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (AppData.UserOrder != null)
                {
                    int count = TotalCount(AppData.Context.OrderOfFoods.ToList().Where(p => p.Order == AppData.UserOrder).ToList());
                    AppData.Context.ChangeTracker.Entries<OrderOfFood>().Where(p => p.Entity.Order == AppData.UserOrder).ToList().ForEach(p => p.Reload());
                    if (count > 0)
                    {
                        ElipseFood.Visibility = Visibility.Visible;
                        TblockCountFood.Text = $"{count}";
                    }
                    else
                    {
                        ElipseFood.Visibility = Visibility.Hidden;
                    }
                }
            }
            catch { }


        }
        private int TotalCount(List<OrderOfFood> list)
        {
            int count = 0;
            foreach (var food in list)
            {
                count += Convert.ToInt32(food.Count);
            }
            return count;
        }

        private void TblockBack_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (AppData.MainFrame.CanGoBack)
            {
                AppData.MainFrame.GoBack();
            }
        }

        private void MainFrame_ContentRendered(object sender, EventArgs e)
        {
            string title = (AppData.MainFrame.Content as Page).Title;
            if (AppData.CurrentUser == null)
            {
                GridUserNull.Visibility = Visibility.Visible;
                GridUserNotNull.Visibility = Visibility.Hidden;
            }
            else
            {
                GridUserNull.Visibility = Visibility.Hidden;
                GridUserNotNull.Visibility = Visibility.Visible;
            }
            if (title == "AutorizaionPage")
            {
                BtnLogin.Visibility = Visibility.Collapsed;
            }
            else
            {
                BtnLogin.Visibility = Visibility.Visible;
            }

            if (title == "MenuPage")
            {
                TblockBack.Visibility = Visibility.Hidden;
                TblockBack1.Visibility = Visibility.Hidden;
            }
            else
            {
                TblockBack.Visibility = Visibility.Visible;
                TblockBack1.Visibility = Visibility.Visible;
            }

            TglBtnProfile.IsChecked = false;
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            AppData.MainFrame.Navigate(new AutorizaionPage());
        }

        private void PopupProfileInfo_Opened(object sender, EventArgs e)
        {

        }

        private void PopupProfileInfo_Closed(object sender, EventArgs e)
        {

        }

        private void TbkProfile_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void TbkEditProfile_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void TbkLogout_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Logout();
        }

        private static void Logout()
        {
            MessageBoxResult result = MessageBox.Show("Вы уверены, что хотите выйти из аккаунта?", "Выход", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {

                AppData.CurrentUser = null;

                Properties.Settings.Default.Reset();

                AppData.MainFrame.Navigate(new MenuPage());
            }
        }

        private void TglBtnProfile_Checked(object sender, RoutedEventArgs e)
        {
            if (TglBtnProfile.IsChecked == true)
                PopupProfileInfo.IsOpen = true;
            else
                PopupProfileInfo.IsOpen = false;

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void TbkOrder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AppData.MainFrame.Navigate(new FoodFromOrderPage());
        }
    }
}
