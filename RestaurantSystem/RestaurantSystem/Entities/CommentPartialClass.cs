﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantSystem.Entities
{
    public partial class Comment
    {

        public string StarsCount
        {
            get
            {
                switch (Rating)
                {
                    case 1:
                        return "\xE735" + " \xE734" + " \xE734" + " \xE734" + " \xE734";
                    case 2:
                        return "\xE735" + " \xE735" + " \xE734" + " \xE734" + " \xE734";
                    case 3:
                        return "\xE735" + " \xE735" + " \xE735" + " \xE734" + " \xE734";
                    case 4:
                        return "\xE735" + " \xE735" + " \xE735" + " \xE735" + " \xE734";
                    case 5:
                        return "\xE735" + " \xE735" + " \xE735" + " \xE735" + " \xE735";
                    default: 
                        return null;                       
                }
            }
            set { }
        }
    }
}
