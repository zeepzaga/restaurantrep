﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace RestaurantSystem.Entities
{
    public partial class CategoryOfTable
    {
        public BitmapImage Photo
        {
            get
            {
                switch (Name)
                {
                    case "2-х местный стол":
                        return new BitmapImage(new Uri(@"/RestaurantSystem;component/Resources/two-seated table.png", UriKind.RelativeOrAbsolute));
                    case "4-х местный стол":
                        return new BitmapImage(new Uri(@"/RestaurantSystem;component/Resources/four-seated table.png", UriKind.RelativeOrAbsolute));
                    case "Банкетный стол":
                        return new BitmapImage(new Uri(@"/RestaurantSystem;component/Resources/banquet table.png", UriKind.RelativeOrAbsolute));
                    default:
                        return null;
                }
            }
        }
    }
}
