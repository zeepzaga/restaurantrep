﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace RestaurantSystem.Entities
{
    public class AppData
    {
        public static RestaurantEntities Context = new RestaurantEntities();
        public static User CurrentUser;
        public static Frame MainFrame;
        public static Order UserOrder;
    }
}
