﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantSystem.Entities
{
    public partial class Food
    {

        public string Mass
        {
            get
            {
                if (CategoryOfFoodId == 8 || CategoryOfFoodId == 9)
                {
                    return $"{Weight} мл.";
                }
                return $"{Weight} г.";
            }
            set { }
        }
    }
}
