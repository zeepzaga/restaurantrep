//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RestaurantSystem.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Table
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Table()
        {
            this.Reservations = new HashSet<Reservation>();
        }
    
        public string Id { get; set; }
        public string Desciption { get; set; }
        public int CountPlace { get; set; }
        public int StatusOfTableId { get; set; }
        public decimal HourlyPrice { get; set; }
        public int CategoryOfTableId { get; set; }
        public Nullable<int> CoordinateX { get; set; }
        public Nullable<int> CoordinateY { get; set; }
    
        public virtual CategoryOfTable CategoryOfTable { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reservation> Reservations { get; set; }
        public virtual StatusOfTable StatusOfTable { get; set; }
    }
}
