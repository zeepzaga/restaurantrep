﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using RestaurantSystem.Entities;

namespace RestaurantSystem.Pages
{
    /// <summary>
    /// Логика взаимодействия для FoodFromOrderPage.xaml
    /// </summary>
    public partial class FoodFromOrderPage : Page
    {
        List<OrderOfFood> orderOfFoodsFirst = AppData.Context.OrderOfFoods.ToList().Where(p => p.Order == AppData.UserOrder).ToList();
        public FoodFromOrderPage()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += Timer_Tick;
            timer.Interval = new TimeSpan(0,0,0,0,500);
            timer.Start();
            ICFoodList.ItemsSource = orderOfFoodsFirst;

        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            AppData.Context.ChangeTracker.Entries<OrderOfFood>().Where(p => p.Entity.Order == AppData.UserOrder).ToList().ForEach(p => p.Reload());
            List<OrderOfFood> list = AppData.Context.OrderOfFoods.ToList().Where(p => p.Order == AppData.UserOrder).ToList();
            TbSumPrice.Text = $"{SumPrice(list)}₽ ";
            TblockAllCount.Text = $"товаров: {TotalCount(list)}";
            if (list.Count < orderOfFoodsFirst.Count)
            {
                orderOfFoodsFirst = list;
                ICFoodList.ItemsSource = null;
                ICFoodList.ItemsSource = orderOfFoodsFirst;
            }
        }
        private double SumPrice(List<OrderOfFood> list)
        {
            double sum = 0;

            foreach (var food in list)
            {
                double sumFoodPrice = Convert.ToDouble(food.Count) * Convert.ToDouble(food.Food.Price);
                sum += sumFoodPrice;
            }
            return sum;
        }
        private int TotalCount(List<OrderOfFood> list)
        {
            int count = 0;
            foreach (var food in list)
            {
                count += Convert.ToInt32(food.Count);
            }
            return count;
        }

        private void BtnPay_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Товар оплачен", "Уведомление", MessageBoxButton.OK, MessageBoxImage.Information);
            AppData.UserOrder.StatusOfOrderId = 3;
            AppData.Context.SaveChanges();
        }
    }
}
