﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RestaurantSystem.Entities;

namespace RestaurantSystem.Pages.Admin
{
    /// <summary>
    /// Логика взаимодействия для TableEditPage.xaml
    /// </summary>
    public partial class TableEditPage : Page
    {
        List<Entities.Table> TablesList = new List<Entities.Table>();
        bool _isDown = false;

        public TableEditPage()
        {
            InitializeComponent();
            LVTableList.ItemsSource = AppData.Context.CategoryOfTables.ToList();
        }

        private void LVTableList_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CategoryOfTable category = (CategoryOfTable)LVTableList.SelectedItem as CategoryOfTable;


                TablesList.Add(new Entities.Table
                {
                    CategoryOfTable = category,
                });  
                DataContext = TablesList.ToList().FirstOrDefault(i => i.CategoryOfTable == category);            

            Image image = new Image();
            image.Width = 100;
            image.Height = 100;
            image.Source = category.Photo;
            image.MouseMove += Image_MouseMove;
            image.MouseDown += Image_MouseDown;
            image.MouseUp += Image_MouseUp;
            RenderOptions.SetBitmapScalingMode(image, BitmapScalingMode.HighQuality);
            CnsMap.Children.Add(image);
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _isDown = false;
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Image_MouseMove(sender, e);
            _isDown = true;
        }

        private void Image_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isDown)
            {
                var windowPosition = Mouse.GetPosition(CnsMap);
                double PositionY = windowPosition.Y - (sender as Image).ActualHeight / 2;
                double PositionX = windowPosition.X - (sender as Image).ActualWidth / 2;

                if (PositionY <= 0)
                    PositionY = 0;
                if (PositionX <= 0)
                    PositionX = 0;
                if (windowPosition.Y >= CnsMap.ActualHeight - (sender as Image).ActualHeight / 2)
                    PositionY = CnsMap.ActualHeight - (sender as Image).ActualHeight;
                if (windowPosition.X >= CnsMap.ActualWidth - (sender as Image).ActualWidth / 2)
                    PositionX = CnsMap.ActualWidth - (sender as Image).ActualWidth;

                Canvas.SetTop(sender as Image, PositionY);
                Canvas.SetLeft(sender as Image, PositionX);
            }
        }

        private void btnApplyTableEdit_Click(object sender, RoutedEventArgs e)
        {
            if (CnsMap.Children.Count != 0)
            {

            }
        }
    }
}
