﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RestaurantSystem.Entities;

namespace RestaurantSystem.Pages.Admin
{
    /// <summary>
    /// Логика взаимодействия для AddWorkerPage.xaml
    /// </summary>
    public partial class AddWorkerPage : Page
    {
        bool ready = false;
        public AddWorkerPage()
        {
            InitializeComponent();
            CbRole.ItemsSource = AppData.Context.Roles.ToList();
            CbRole.SelectedIndex = -1;
        }

        private void BtnNextStep_Click(object sender, RoutedEventArgs e)
        {
            if (ready == true)
            {
                var user = AppData.Context.Users.Add(new User()
                {
                    Login = TbLogin.Text,
                    Password = PbPassword.Password,
                    Role = CbRole.SelectedItem as Role
                });
                AppData.Context.SaveChanges();

                AppData.Context.Workers.Add(new Worker()
                {
                    User = user,
                    FirstName = TbFirstName.Text,
                    LastName = TbLastName.Text,
                    Patronymic = TbPatronymic.Text,
                    TelephoneNumber = TbTelephoneNumber.Text,
                    PassportNumber = TbPassportNumber.Text,
                    PassportSeria = TbPassportSeria.Text,
                    DateOfBirthDay = DPDateOfBirthDay.SelectedDate.Value.Date,
                    Salary = Convert.ToDecimal(TbSalary.Text)
                });
                AppData.Context.SaveChanges();

                MessageBox.Show("Работник успешно зарегистрирован!", "Успех", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                AppData.MainFrame.Navigate(new MenuPage());
            }

            MainTabControl.SelectedIndex = 1;
            TIContactInfo.Visibility = Visibility.Hidden;
            TIPersonalInfo.Visibility = Visibility.Visible;
            UpdateButtons();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainTabControl.SelectedIndex = 0;
            TIContactInfo.Visibility = Visibility.Visible;
            TIPersonalInfo.Visibility = Visibility.Hidden;
            ready = false;
            UpdateButtons();
        }

        private void TbLogin_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (AppData.Context.Users.ToList().FirstOrDefault(i => i.Login == TbLogin.Text) != null)
                TbkLoginCheck.Visibility = Visibility.Visible;
            else
                TbkLoginCheck.Visibility = Visibility.Hidden;
            UpdateButtons();
        }

        private void PbPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (PbPassword.Password != PbRepeatPassword.Password && !string.IsNullOrWhiteSpace(PbRepeatPassword.Password))
                TbkPasswordCheck.Visibility = Visibility.Visible;

            else
                TbkPasswordCheck.Visibility = Visibility.Hidden;

            UpdateButtons();
        }

        private void PbRepeatPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (PbPassword.Password != PbRepeatPassword.Password)
                TbkPasswordCheck.Visibility = Visibility.Visible;

            else
                TbkPasswordCheck.Visibility = Visibility.Hidden;

            UpdateButtons();
        }

        private void CbRole_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateButtons();
        }

        private void UpdateButtons()
        {
            if (TIContactInfo.IsVisible && !string.IsNullOrWhiteSpace(TbLogin.Text) && CbRole.SelectedIndex != -1 && !TbkPasswordCheck.IsVisible
               && !string.IsNullOrWhiteSpace(PbPassword.Password) && !string.IsNullOrWhiteSpace(PbRepeatPassword.Password) && !TbkLoginCheck.IsVisible)
            {
                ready = false;
                BtnNextStep.IsEnabled = true;
            }
            else if (TIPersonalInfo.IsVisible && !string.IsNullOrWhiteSpace(TbLastName.Text) && DPDateOfBirthDay.SelectedDate != null && !string.IsNullOrWhiteSpace(TbSalary.Text)
                 && !string.IsNullOrWhiteSpace(TbFirstName.Text) && !string.IsNullOrWhiteSpace(TbPassportSeria.Text) && !string.IsNullOrWhiteSpace(TbPassportNumber.Text))
            {
                BtnNextStep.IsEnabled = true;
                ready = true;
            }
            else
            {
                ready = false;
                BtnNextStep.IsEnabled = false;
            }
        }

        private void TIContactInfo_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (TIContactInfo.IsVisible)
            {
                TblockElipse1.Visibility = Visibility.Visible;
                TblockElipse1_1.Visibility = Visibility.Hidden;
                Elipse2.Fill = Brushes.Gray;
                BtnNextStep.Content = "Далее";
                BtnBack.IsEnabled = false;
            }
        }

        private void TIPersonalInfo_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (TIPersonalInfo.IsVisible)
            {
                TblockElipse1.Visibility = Visibility.Hidden;
                TblockElipse1_1.Visibility = Visibility.Visible;
                BtnNextStep.Content = "Зарегистрироваться";
                Elipse2.Fill = Brushes.Transparent;
                BtnBack.IsEnabled = true;
            }
        }

        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            VbxTab.MaxHeight = this.ActualHeight / 8;
            if (VbxTab.MaxHeight > 105)
                VbxTab.MaxHeight = 105;
        }

        private void TbSalary_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Any(c => !char.IsDigit(c)))
                e.Handled = true;
        }

        private void TbTelephoneNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Any(c => !char.IsDigit(c)))
                e.Handled = true;
        }

        private void TbPassportNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Any(c => !char.IsDigit(c)))
                e.Handled = true;
        }

        private void TbPassportSeria_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Any(c => !char.IsDigit(c)))
                e.Handled = true;
        }

        private void TbPassportNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateButtons();
        }

        private void TbTelephoneNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateButtons();
        }

        private void TbSalary_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateButtons();
        }

        private void TbFirstName_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateButtons();
        }

        private void TbPatronymic_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateButtons();
        }

        private void DPDateOfBirthDay_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateButtons();
        }
    }
}
