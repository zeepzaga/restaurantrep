﻿using RestaurantSystem.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Pages.Admin
{
    /// <summary>
    /// Логика взаимодействия для MenuPage.xaml
    /// </summary>
    public partial class MenuPage : Page
    {
        public MenuPage()
        {
            InitializeComponent();
        }


        private void BtnFoodMenuList_Click(object sender, RoutedEventArgs e)
        {
            AppData.MainFrame.Navigate(new FoodMenuListPage());
        }

        private void BtnAddFood_Click(object sender, RoutedEventArgs e)
        {
            AppData.MainFrame.Navigate(new AddEditFood(null));
        }

        private void BtnAddWorker_Click(object sender, RoutedEventArgs e)
        {
            AppData.MainFrame.Navigate(new AddWorkerPage());
        }

        private void BtnWorkerList_Click(object sender, RoutedEventArgs e)
        {
            AppData.MainFrame.Navigate(new WorkersPage());

        }

        private void BtnEditTable_Click(object sender, RoutedEventArgs e)
        {
            AppData.MainFrame.Navigate(new TableEditPage());
        }
    }
}
