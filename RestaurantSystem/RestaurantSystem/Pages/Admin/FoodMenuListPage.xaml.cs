﻿using RestaurantSystem.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Pages.Admin
{
    /// <summary>
    /// Логика взаимодействия для FoodMenuListPage.xaml
    /// </summary>
    public partial class FoodMenuListPage : Page
    {
        private List<Food> _foodList = new List<Food>();
        BackgroundWorker bw;
        public FoodMenuListPage()
        {
            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += Bw_DoWork;
            bw.ProgressChanged += Bw_ProgressChanged;
            bw.RunWorkerAsync(null);
            Dispatcher.Invoke(new Action(() => FirstData()));
            InitializeComponent();

        }
        private async void FirstData()
        {
            await Task.Run(() => _foodList = AppData.Context.Foods.ToList());
            if (CbCategory.Items.Count == 0)
            {
                List<CategoryOfFood> categoryOfFoods = AppData.Context.CategoryOfFoods.ToList();
                categoryOfFoods.Insert(0, new CategoryOfFood
                {
                    Name = "Все категории"
                });
                CbCategory.SelectedIndex = 0;
                CbCategory.ItemsSource = categoryOfFoods;
            }
            UpdateDataGrid(_foodList);
        }

        private void Bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (e.ProgressPercentage)
            {
                case 0:
                    TblockDot.Text = "";
                    break;
                case 1:
                    TblockDot.Text = ".";
                    break;
                case 2:
                    TblockDot.Text = "..";
                    break;
                case 3:
                    TblockDot.Text = "...";
                    break;
                default:
                    break;
            }
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = -1; i <= 3; i++)
            {
                bw.ReportProgress(i);
                if (i == 3)
                {
                    i = -1;
                }
                Thread.Sleep(400);
            }
        }

        private void TbName_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateDataGrid(_foodList);

        }

        private void CbCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateDataGrid(_foodList);
        }

        private void BtnChange_Click(object sender, RoutedEventArgs e)
        {
            AppData.MainFrame.Navigate(new AddEditFood((sender as Button).DataContext as Food));
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите удалить это продукт", "Вы уверены", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                AppData.Context.Foods.Remove((sender as Button).DataContext as Food);
                AppData.Context.SaveChanges();
            }
            UpdateDataGrid(_foodList);
        }
        private void UpdateDataGrid(List<Food> foodList)
        {
            foodList = _foodList.Where(p => p.Name.ToLower().Contains(TbName.Text.ToLower())).ToList();
            if (CbCategory.SelectedIndex > 0)
            {
                foodList = _foodList.Where(p => p.Name.ToLower().Contains(TbName.Text.ToLower())
                && p.CategoryOfFood == CbCategory.SelectedItem as CategoryOfFood).ToList();
            }
            DgFoodMenu.ItemsSource = foodList;
            DgFoodMenu.Items.Refresh();
            SpWait.Visibility = Visibility.Collapsed;
        }

        private void BtnPrint_Click(object sender, RoutedEventArgs e)
        {
            var currFood = (sender as Button).DataContext as Food;
            AppData.MainFrame.Navigate(new Pages.Admin.PrintRecipePage(currFood));
        }
    }
}
