﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RestaurantSystem.Entities;

namespace RestaurantSystem.Pages.Admin
{
    /// <summary>
    /// Логика взаимодействия для WorkersPage.xaml
    /// </summary>
    public partial class WorkersPage : Page
    {
        public WorkersPage()
        {
            InitializeComponent();

            var collectionWorkers = new ListCollectionView(AppData.Context.Workers.ToList());
            collectionWorkers.GroupDescriptions.Add(new PropertyGroupDescription("User.Role.Description"));

            DGWorkers.ItemsSource = collectionWorkers;
        }
    }
}
