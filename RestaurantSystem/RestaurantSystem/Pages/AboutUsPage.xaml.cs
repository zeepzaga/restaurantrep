﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Pages
{
    /// <summary>
    /// Логика взаимодействия для AboutUsPage.xaml
    /// </summary>
    public partial class AboutUsPage : Page
    {
        public AboutUsPage()
        {
            InitializeComponent();
            List<byte[]> photo = new List<byte[]>();

            //foreach (var photo1 in Directory.GetFiles("C:\\Users\\Sasha\\Desktop\\Фото"))
            //{
            //    photo.Add(File.ReadAllBytes(photo1));
            //}
            //ICImage.ItemsSource = photo.ToList();
        }

        private void BtnScrollImageLeft_Click(object sender, RoutedEventArgs e)
        {
            ScrollViewerr.PageLeft();
        }

        private void BtnScrollImageRight_Click(object sender, RoutedEventArgs e)
        {
            ScrollViewerr.PageRight();
        }

        private void ImgPhoto_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var image = (sender as Image).DataContext;
            Windows.FullPhotoWindow fullPhotoWin = new Windows.FullPhotoWindow(image)
            {
                Owner = MainWindow.GetWindow(this)
            };
            fullPhotoWin.ShowDialog();
        }
    }
}
