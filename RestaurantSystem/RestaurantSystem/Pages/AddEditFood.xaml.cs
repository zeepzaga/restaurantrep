﻿using Microsoft.Win32;
using RestaurantSystem.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddEditFood.xaml
    /// </summary>
    public partial class AddEditFood : Page
    {
        private bool _IsAdult = false;
        private byte[] _photo = null;
        private Food _food;
        public static List<CountOfProduct> listCountOfProduct = new List<CountOfProduct>();
        Recipe recipe;
        bool check = false;


        public AddEditFood(Food food)
        {
            InitializeComponent();
            listCountOfProduct.Clear();
            CbCategory.ItemsSource = AppData.Context.CategoryOfFoods.ToList();
            if (food != null)
            {
                TextBlock.Text = "Редактировать блюдо";
                _food = food;
                LoadingData();
            }
        }

        public AddEditFood()
        {
            InitializeComponent();
            CbCategory.ItemsSource = AppData.Context.CategoryOfFoods.ToList();
        }

        private void TIFood_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (TIFood.IsVisible)
            {
                TblockElipse1.Visibility = Visibility.Visible;
                TblockElipse1_1.Visibility = Visibility.Hidden;
                BtnRecipeSkip.Visibility = Visibility.Collapsed;
                Elipse2.Fill = Brushes.Gray;
                BtnNextStep.Content = "Далее";
                BtnBack.IsEnabled = false;
            }
            else
            {
                TblockElipse1.Visibility = Visibility.Hidden;
                TblockElipse1_1.Visibility = Visibility.Visible;
                BtnRecipeSkip.Visibility = Visibility.Visible;
                BtnNextStep.Content = "Готово";
                Elipse2.Fill = Brushes.Transparent;
                BtnBack.IsEnabled = true;
                UpdateListView();
            }

            check = false;
            CheckBtnNext();
        }

        private void BtnNextStep_Click(object sender, RoutedEventArgs e)
        {
            MainTabControl.SelectedIndex = 1;
            TIFood.Visibility = Visibility.Hidden;
            TIFood1.Visibility = Visibility.Visible;

            if (_food.Recipe == null && BtnNextStep.IsEnabled == true)
            {
                recipe = new Recipe
                {
                    Name = TBRecipeName.Text,
                    Description = TbRecipe.Text
                };

                foreach (var item in listCountOfProduct)
                {
                    recipe.CountOfProducts.Add(item);
                }
                AppData.Context.Recipes.Add(recipe);
                AppData.Context.SaveChanges();

                AddFood();
            }
            else if (check == true)
            {
                recipe.Name = TBRecipeName.Text;
                recipe.Description = TbRecipe.Text;
                recipe.CountOfProducts.Clear();
                foreach (var item in listCountOfProduct)
                {
                    recipe.CountOfProducts.Add(item);
                }
                AddFood();
            }
            check = true;
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainTabControl.SelectedIndex = 0;
            TIFood.Visibility = Visibility.Visible;
            TIFood1.Visibility = Visibility.Hidden;
        }

        private void TbName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(TbName.Text) && !String.IsNullOrWhiteSpace(TbPrice.Text)
                && !String.IsNullOrWhiteSpace(TbWeight.Text) && CbCategory.SelectedIndex > -1)
            {
                BtnNextStep.IsEnabled = true;
            }
        }

        private void BtnAddCategory_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void BtnRecipeSkip_Click(object sender, RoutedEventArgs e)
        {
            AddFood();
        }

        private void AddFood()
        {
            if (RbAdultY.IsChecked == true)
            {
                _IsAdult = true;
            }
            if (RbAdultN.IsChecked == true)
            {
                _IsAdult = false;
            }
            if (_food != null)
            {
                ChangeData();
            }
            else
            {
                AppData.Context.Foods.Add(new Food
                {
                    CategoryOfFood = CbCategory.SelectedItem as CategoryOfFood,
                    Name = TbName.Text,
                    IsForAdult = _IsAdult,
                    Photo = _photo,
                    Weight = Convert.ToInt32(TbWeight.Text),
                    Price = Convert.ToDecimal(TbPrice.Text),
                    Description = String.IsNullOrWhiteSpace(TbDescription.Text) ? null : TbDescription.Text,
                    Recipe = recipe
                });
            }
            AppData.Context.SaveChanges();
            CheckCountOfProduct();
            AppData.MainFrame.Navigate(new Admin.FoodMenuListPage());
        }

        private static void CheckCountOfProduct()
        {
            foreach (var item in AppData.Context.CountOfProducts.ToList().Where(i => i.Recipes.Count() == 0))
            {
                AppData.Context.CountOfProducts.Remove(item);
                AppData.Context.SaveChanges();
            }
        }

        private void LoadingData()
        {
            TbName.Text = _food.Name;
            TbDescription.Text = _food.Description;
            TbPrice.Text = $"{_food.Price}";
            TbWeight.Text = $"{_food.Weight}";
            Img.DataContext = _food.Photo;
            _photo = _food.Photo;
            CbCategory.SelectedItem = _food.CategoryOfFood;
            if (_food.IsForAdult == true)
            {
                RbAdultY.IsChecked = true;
                RbAdultN.IsChecked = false;
                _IsAdult = true;
            }
            else
            {
                RbAdultY.IsChecked = false;
                RbAdultN.IsChecked = true;
                _IsAdult = false;
            }

            try
            {
                TBRecipeName.Text = _food.Recipe.Name;
                TbRecipe.Text = _food.Recipe.Description;
                recipe = _food.Recipe;
                foreach (var item in _food.Recipe.CountOfProducts)
                {
                    listCountOfProduct.Add(item);
                }
            }
            catch
            { }
        }

        private void BtnLoadImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                Img.DataContext = File.ReadAllBytes(openFileDialog.FileName);
                _photo = File.ReadAllBytes(openFileDialog.FileName);
            }
        }
        private void ChangeData()
        {
            _food.Name = TbName.Text;
            _food.Description = String.IsNullOrWhiteSpace(TbDescription.Text) ? null : TbDescription.Text;
            _food.Price = Convert.ToDecimal(TbPrice.Text);
            _food.Weight = Convert.ToInt32(TbWeight.Text);
            _food.Photo = _photo;
            _food.IsForAdult = _IsAdult;
            _food.CategoryOfFood = CbCategory.SelectedItem as CategoryOfFood;
            _food.Recipe = recipe;
            CheckCountOfProduct();
        }

        private void CbCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BtnProductDelete_Click(object sender, RoutedEventArgs e)
        {
            var selectProduct = LVProducts.SelectedItem as CountOfProduct;
            listCountOfProduct.Remove(selectProduct);
            UpdateListView();
        }

        private void BtnProductAdd_Click(object sender, RoutedEventArgs e)
        {
            Windows.AddProductWindow addProductWin = new Windows.AddProductWindow()
            {
                Owner = MainWindow.GetWindow(this)
            };
            addProductWin.ShowDialog();
            UpdateListView();
        }

        private void UpdateListView()
        {
            LVProducts.ItemsSource = listCountOfProduct;
            LVProducts.Items.Refresh();
            CheckBtnNext();
        }

        private void CheckBtnNext()
        {
            if (!string.IsNullOrWhiteSpace(TBRecipeName.Text) && !string.IsNullOrWhiteSpace(TbRecipe.Text)
                && listCountOfProduct.Count != 0 && BtnNextStep.Content.ToString() == "Готово")
                BtnNextStep.IsEnabled = true;
            else if (BtnNextStep.Content.ToString() == "Готово")
                BtnNextStep.IsEnabled = false;
            else if (BtnNextStep.Content.ToString() == "Далее")
                BtnNextStep.IsEnabled = true;

        }

        private void LVProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LVProducts.SelectedIndex == -1)
                BtnProductDelete.IsEnabled = false;
            else
                BtnProductDelete.IsEnabled = true;

            CheckBtnNext();
        }

        private void TBRecipeName_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckBtnNext();
        }

        private void TbRecipe_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckBtnNext();
        }
    }
}
