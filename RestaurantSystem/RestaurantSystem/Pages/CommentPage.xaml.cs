﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RestaurantSystem.Entities;

namespace RestaurantSystem.Pages
{
    /// <summary>
    /// Логика взаимодействия для CommentPage.xaml
    /// </summary>
    public partial class CommentPage : Page
    {
        public CommentPage()
        {
            InitializeComponent();
            ICCommentList.ItemsSource = AppData.Context.Comments.ToList();
        }

        private void TglBtn_Checked(object sender, RoutedEventArgs e)
        {     
            if (TglBtn.IsChecked == true)
                GridComment.Height = 350;
            else
                GridComment.Height = 110;

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Check();
        }

        private void Check()
        {
            if (Controls.RatingControl.selected != 0 && !string.IsNullOrWhiteSpace(TbxComment.Text))
            {
                BtnSendComment.IsEnabled = true;
            }
            else
            {
                BtnSendComment.IsEnabled = false;
            }
        }

        private void RatingControl_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            Check();
        }

        private void BtnSendComment_Click(object sender, RoutedEventArgs e)
        {
            AppData.Context.Comments.Add(new Comment()
            {
                Comment1 = TbxComment.Text,
                Client = AppData.Context.Clients.ToList().FirstOrDefault(i => i.User == AppData.CurrentUser),
                Rating = Controls.RatingControl.selected,
                Worker = CbxWorker.SelectedItem as Worker,
                Order = CbxNumber.SelectedItem as Order
            });
            AppData.Context.SaveChanges();
        }

        private void TglBtn_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (AppData.CurrentUser == null)
            {
                MessageBox.Show("Войдите в аккаунт, чтобы оставить отзыв.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                TglBtn.IsChecked = false;
            }
        }
    }
}
