﻿using RestaurantSystem.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Pages
{
    /// <summary>
    /// Логика взаимодействия для AutorizaionPage.xaml
    /// </summary>
    public partial class AutorizaionPage : Page
    {
        private string _error;
        public AutorizaionPage()
        {
            InitializeComponent();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            _error = "";
            if (String.IsNullOrWhiteSpace(PbPassword.Password) || String.IsNullOrWhiteSpace(TbLogin.Text))
            {
                if (String.IsNullOrWhiteSpace(TbLogin.Text)) _error += "•Введите логин\n";
                if (String.IsNullOrWhiteSpace(PbPassword.Password)) _error += "•Введите пароль\n";
                MessageBox.Show(_error, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                AppData.CurrentUser = AppData.Context.Users.ToList().FirstOrDefault(p => p.Login == TbLogin.Text && p.Password == PbPassword.Password);
                AppData.Context.SaveChanges();
                if (CbIsRemember.IsChecked == true)
                {
                    Properties.Settings.Default.login = TbLogin.Text;
                    Properties.Settings.Default.password = PbPassword.Password;
                    Properties.Settings.Default.Save();
                }
                if (AppData.CurrentUser != null)
                {
                    switch (AppData.CurrentUser.RoleId)
                    {
                        case "A":
                            AppData.MainFrame.Navigate(new Admin.MenuPage());
                            return;
                        case "M":
                            return;
                        case "Cl":
                            AppData.MainFrame.Navigate(new Client.MenuPage());
                            AppData.UserOrder = AppData.Context.Orders.FirstOrDefault(p => p.Client.User.Id == AppData.CurrentUser.Id && p.StatusOfOrderId == 1);
                            return;
                        case "Ch":
                            return;
                    }

                }
                MessageBox.Show("Не верный логин или пароль", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void TBlockRegistration_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AppData.MainFrame.Navigate(new Pages.Client.RegistrationPage());
        }
    }
}
