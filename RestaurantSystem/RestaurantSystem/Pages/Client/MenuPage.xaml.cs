﻿using RestaurantSystem.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Pages.Client
{
    /// <summary>
    /// Логика взаимодействия для MenuPage.xaml
    /// </summary>
    public partial class MenuPage : Page
    {
        public MenuPage()
        {
            InitializeComponent();
            ICPopularFood.ItemsSource = AppData.Context.Foods.Take(6).ToList();
        }

        private void TblockAboutUs_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AppData.MainFrame.Navigate(new AboutUsPage());
        }

        private void TblockMenu_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AppData.MainFrame.Navigate(new FoodMenuPage());
        }

        private void TblockTable_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void TblockBonusCard_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void TblockCommentPage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AppData.MainFrame.Navigate(new CommentPage());
        }
    }
}
