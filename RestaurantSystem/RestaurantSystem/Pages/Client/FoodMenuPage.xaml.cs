﻿using RestaurantSystem.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Pages.Client
{
    /// <summary>
    /// Логика взаимодействия для FoodMenuPage.xaml
    /// </summary>
    public partial class FoodMenuPage : Page
    {
        private List<Food> foods = new List<Food>();
        List<IGrouping<string, Food>> result = new List<IGrouping<string, Food>>();
        BackgroundWorker bw;
        public FoodMenuPage()
        {
            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += Bw_DoWork;
            bw.ProgressChanged += Bw_ProgressChanged;
            bw.RunWorkerAsync(null);
            Dispatcher.Invoke(new Action(() => FirstData()));
            InitializeComponent();
        }
        private async void FirstData()
        {
            await Task.Run(() => foods = AppData.Context.Foods.ToList());
            if (CbCategory.Items.Count == 0)
            {
                List<CategoryOfFood> categoryOfFoods = AppData.Context.CategoryOfFoods.ToList();
                categoryOfFoods.Insert(0, new CategoryOfFood
                {
                    Name = "Все категории"
                });
                CbCategory.SelectedIndex = 0;
                CbCategory.ItemsSource = categoryOfFoods;
            }
            UpdateFoodMenuList();
        }

        private void Bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (e.ProgressPercentage)
            {
                case 0:
                    TblockDot.Text = "";
                    break;
                case 1:
                    TblockDot.Text = ".";
                    break;
                case 2:
                    TblockDot.Text = "..";
                    break;
                case 3:
                    TblockDot.Text = "...";
                    break;
                default:
                    break;
            }
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = -1; i <= 3; i++)
            {
                bw.ReportProgress(i);
                if (i == 3)
                {
                    i = -1;
                }
                Thread.Sleep(400);
            }
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            TbName.Text = "";
            CbCategory.SelectedIndex = 0;
        }

        private void TbName_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateFoodMenuList();
            Scroll.ScrollToTop();
        }

        private void CbCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateFoodMenuList();
            Scroll.ScrollToTop();
        }
        private void UpdateFoodMenuList()
        {
            result.Clear();
            if (CbCategory.SelectedIndex > 0)
            {
                result = foods.Where(p => p.Name.ToLower().Contains(TbName.Text.ToLower()) && p.CategoryOfFood == CbCategory.SelectedItem as CategoryOfFood).GroupBy(p => p.CategoryOfFood.Name).ToList();
            }
            else
            {
                result = foods.Where(p => p.Name.ToLower().Contains(TbName.Text.ToLower())).GroupBy(p => p.CategoryOfFood.Name).ToList();
            }

            ICFoodMenu.ItemsSource = result;
            ICFoodMenu.Items.Refresh();
            SpWait.Visibility = Visibility.Hidden;

        }

        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ViewBox.Height = this.ActualHeight / 10;
        }
    }
}
