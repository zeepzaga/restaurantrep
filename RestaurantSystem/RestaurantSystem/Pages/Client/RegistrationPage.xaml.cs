﻿using RestaurantSystem.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantSystem.Pages.Client
{
    /// <summary>
    /// Логика взаимодействия для RegistrationPage.xaml
    /// </summary>
    public partial class RegistrationPage : Page
    {
        string _error;
        User _user;
        public RegistrationPage()
        {
            InitializeComponent();
        }

        private void BtnRegistration_Click(object sender, RoutedEventArgs e)
        {
            _error = "";
            if (String.IsNullOrWhiteSpace(TbLogin.Text)) _error += "•Введите логин\n";
            else if (AppData.Context.Users.ToList().FirstOrDefault(i => i.Login == TbLogin.Text) != null) _error += "•Пользователь с таким логином существует\n";
            if (String.IsNullOrWhiteSpace(PbPassword.Password)) _error += "•Введите пароль\n";
            else if (String.IsNullOrWhiteSpace(PbRepeatPassword.Password)) _error += "•Повторите пароль\n";
            else if (PbPassword.Password != PbRepeatPassword.Password) _error += "•Пароли не совпадают\n";
            if (String.IsNullOrWhiteSpace(TbFirstName.Text)) _error += "•Введите имя\n";
            if (String.IsNullOrWhiteSpace(TbLastName.Text)) _error += "•Введите фамилия\n";
            if (DpDateOfBirthDay.SelectedDate.Value == null) _error += "•Выберите дату рождения\n";
            if (String.IsNullOrWhiteSpace(_error))
            {
                MessageBox.Show(_error, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            AppData.Context.Users.Add(_user = new User
            {
                Login = TbLogin.Text,
                Password = PbPassword.Password,
                RoleId = "Cl"
            });
            AppData.Context.SaveChanges();
            AppData.Context.Clients.Add(new Entities.Client
            {
                User = _user,
                FirstName = TbFirstName.Text,
                LastName = TbLastName.Text,
                DateOfBirthDay = DpDateOfBirthDay.SelectedDate.Value,
            });
            AppData.Context.SaveChanges();
            MessageBox.Show("Вы успешно зарегестрировались", "Поздравляем!", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void TblockLogin_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AppData.MainFrame.Navigate(new MenuPage());
        }
    }
}
