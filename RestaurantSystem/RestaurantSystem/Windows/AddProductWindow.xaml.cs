﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RestaurantSystem.Entities;

namespace RestaurantSystem.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddProductWindow.xaml
    /// </summary>
    public partial class AddProductWindow : Window
    {
        public AddProductWindow()
        {
            InitializeComponent();
            CbxProduct.ItemsSource = AppData.Context.Products.ToList();
        }

        private void TbCount_TextChanged(object sender, TextChangedEventArgs e)
        {
            Update();
        }

        private void CbxProduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Update();
        }

        private void Update()
        {
            if (!string.IsNullOrWhiteSpace(TbCount.Text) && CbxProduct.SelectedIndex != -1)
                BtnAdd.IsEnabled = true;
            else
                BtnAdd.IsEnabled = false;
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (Pages.AddEditFood.listCountOfProduct.ToList().FirstOrDefault(i => i.Product == CbxProduct.SelectedItem as Product) == null)
            {
                Pages.AddEditFood.listCountOfProduct.Add(new CountOfProduct()
                {
                    Product = CbxProduct.SelectedItem as Product,
                    Count = Convert.ToDecimal(TbCount.Text)
                });
                Close();
            }
            else
            {
                MessageBox.Show("Вы уже добавили этот продукт. Чтобы изменить количество или убрать " +
                    "продукт из рецепта, закройте это окно и в разделе 'Необходимые продукты' выберите и затем удалите продукт по нажатию на кнопку '-'.");
            }
        }

        private void TbkClose_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Close();
        }
    }
}
