﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RestaurantSystem.Windows
{
    /// <summary>
    /// Логика взаимодействия для FullPhotoWindow.xaml
    /// </summary>
    public partial class FullPhotoWindow : Window
    {
        public FullPhotoWindow(object image)
        {
            InitializeComponent();
            ImgFull.DataContext = image;
        }

        private void TbkClose_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
    }
}
